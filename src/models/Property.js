
function Property(fullAddress, propertyType, yearBuilt, lotSize, finishedSize, bathrooms, bedrooms, zpid, zestimateAmount, valuationRange, valuationChange, lastUpdated) {
    this.fullAddress = fullAddress;
    this.propertyType = propertyType;
    this.yearBuilt = yearBuilt;
    this.lotSize = lotSize;
    this.finishedSize = finishedSize;
    this.bathrooms = bathrooms;
    this.bedrooms = bedrooms;
    this.zpid = zpid;
    this.lastUpdated = lastUpdated;
    this.zestimateAmount = zestimateAmount;
    this.valuationRange = valuationRange;
    this.valuationChange = valuationChange;
}

export default Property
