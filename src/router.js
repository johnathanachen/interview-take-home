import Vue from 'vue'
import Router from 'vue-router'
import Search from './components/Search'
import Playground from './views/Playground'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/', 
      redirect: {
        name: 'search'
    }
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/playground', 
      name: 'playground',
      component: Playground
    }
  ]
})
