import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// ================================
// Getting familiar with Vuex 
// ================================

export default new Vuex.Store({
  state: {
    counter: 0,
    error: false
  },
  // show data
  getters: {
    tripleCounter: state => {
      return state.counter * 3
    }
  },
  // mutate state  
  mutations: {
    increment: (state, num) => {
      state.counter += num;
    },
    decrement: (state, num) => {
      state.counter -= num;
    }
  },
  // commits the mutation  
  actions: {
    incrementAsync ({ commit }, num) {
      setTimeout(() => {
        commit('increment', num)
      }, 1000)
    }
  }
})
